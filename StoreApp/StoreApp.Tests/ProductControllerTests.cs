﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using StoreApp.Controllers;
using StoreApp.Models;

namespace StoreApp.Tests
{
    [TestFixture]
    public class ProductControllerTests
    {
        [Test]
        public void ProductController_SuccessfullTest() 
        {
            Assert.IsTrue(true);
        }

        [Test]
        public void ProductController_GetAllProducts() 
        {
            var productController = new ProductController();
            var products = productController.GetProducts();

            Assert.IsInstanceOf<IEnumerable<Product>>(products);

            var expectedProductsCount = 5;
            Assert.AreEqual(expectedProductsCount, products.Count());            
        }

        [Test]
        public void ProductContoller_GetProductById_ProductFound() 
        {
            var productController = new ProductController();
            var productId = 1;
            var product = productController.GetProduct(productId);

            Assert.IsNotNull(product);
            Assert.AreEqual(productId, product.Id);
        }

        [Test]
        public void ProductController_GetProductById_ProductNotFound ()
        {
            var productController = new ProductController();
            var productId = -1;
            var product = productController.GetProduct(productId);

            Assert.IsNull(product);
        }

        [Test]
        public void Successfultest2() 
        {
            Assert.IsTrue(true);
        }

        //[Test]
        //public void FailingTest() 
        //{
        //    Assert.IsTrue(false);
        //}
    }
}
