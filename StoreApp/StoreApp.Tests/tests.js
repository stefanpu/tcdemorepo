﻿/// <reference path="e:\projects\github\demonstration repositories\storeapp_stefanpu\storeapp\storeapp.tests\scripts\jasmine.js" />
/// <reference path="e:\projects\github\demonstration repositories\storeapp_stefanpu\storeapp\storeapp.tests\logic.js" />
/// <reference path="e:\projects\github\demonstration repositories\storeapp_stefanpu\storeapp\storeapp\scripts\app.js"/>

describe('"Logic.js" Tests', function () {
    it('should increment number', function () {
        expect(increment(1, 0)).toBe(1);
        expect(increment(1, 1)).toBe(2);
        expect(increment(1, 2)).toBe(3);
    });

    it('should return NaN when \'number\' is string', function () {        
        expect(increment('1', 1)).toBeNaN();
    });

    it('should return NaN when \'incr\' is string', function () {        
        expect(increment(1, '')).toBeNaN();
    });
});

describe('"app.js" Tests', function () {
    it('should return 0', function () {
        expect(countWhiteSpaces(null)).toEqual(0);
        expect(countWhiteSpaces('')).toEqual(0);
        expect(countWhiteSpaces(undefined)).toEqual(0);
        expect(countWhiteSpaces('adasdasdad')).toEqual(0);
    });

    it('should return 1', function () {
        expect(countWhiteSpaces(' ')).toEqual(1);
        expect(countWhiteSpaces('aa ')).toEqual(1);
        expect(countWhiteSpaces(' aa')).toEqual(1);
    });

    it('should return 3', function () {
        expect(countWhiteSpaces('   ')).toEqual(3);
        expect(countWhiteSpaces(' a a ')).toEqual(3);
        expect(countWhiteSpaces('   aa')).toEqual(3);
        expect(countWhiteSpaces('aa   ')).toEqual(3);        
    });

    it('should return 2', function () {
        expect(countWhiteSpaces('  ')).toEqual(2);
        expect(countWhiteSpaces('aa  ')).toEqual(2);
        expect(countWhiteSpaces('  aa')).toEqual(2);
    });
});