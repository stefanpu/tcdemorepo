﻿function increment(number, incr)
{
    if (isNaN(number) || number === null || number.length !== undefined) {
        return NaN;
    }

    if (isNaN(incr) || incr === null || incr.length !== undefined) {
        return NaN;
    }

    number += incr;
    return number;
}
