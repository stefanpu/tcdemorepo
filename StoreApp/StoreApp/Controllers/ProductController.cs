﻿using StoreApp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace StoreApp.Controllers
{
    [RoutePrefix("api")]
    public class ProductController : ApiController
    {
        private List<Product> products =
            new List<Product>(
                new Product[]  
                {
                    new Product
                    {
                        Id=1,
                        Price = 12,
                        Description = "Chocolate",
                        Category = "Food"
                    },
                    new Product
                    {
                        Id=2,
                        Price = 13,
                        Description = "Banana",
                        Category = "Food"
                    },
                    new Product
                    {
                        Id=3,
                        Price = 11,
                        Description = "Pencil",
                        Category = "Office Stuff"
                    },
                    
                    new Product
                    {
                        Id=4,
                        Price = 17,
                        Description = "Luxury Pencil",
                        Category = "Office Stuff"
                    },                    
                    new Product
                    {
                        Id=5,
                        Price = 27,
                        Description = "Super Luxury Pencil",
                        Category = "Office Stuff"
                    }
                });


        public IEnumerable<Product> GetProducts()
        {
            return this.products;            
        }

        public Product GetProduct(int id) 
        {
            return this.products.FirstOrDefault(p => p.Id == id);
        }

        public IEnumerable<Product> GetProductsByCategory(string category) 
        {
            return products.Where(p => p.Category.ToLower() == category.ToLower());
        }
                
        public IEnumerable<Product> GetProductsByDescription(string description) 
        {
            return this.products.Where(p => p.Description == description);
        }


        public IEnumerable<Product> GetProductByPrice(decimal price) 
        {
            return this.products.Where(p => p.Price == price);
        }

        public void AddProduct(Product product) 
        {
            // change product Id to the length of the 'products' with the new product
            if (product == null)
            {
                throw new ArgumentNullException("Product can not be null.");
            }

            if (product.Id<=this.products.Count)
            {
                product.Id = this.products.Count + 1;    
            }
            
            products.Add(product);
        }

        [HttpPut]
        public void UpdateProduct(Product newProduct) 
        {
            var product = this.products.FirstOrDefault(p => p.Id == newProduct.Id);

            if (product !=null)
            {
                product.Category = newProduct.Category;
                product.Description = newProduct.Description;
                product.Price = newProduct.Price;                
            }
        }
    }
}
