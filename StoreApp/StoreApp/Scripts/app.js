﻿function countWhiteSpaces(input) {
    var i = 0, counter = 0;

    if (input && input.length) {
        for (; i < input.length; i++) {
            if (input[i] === ' ') {
                counter++;
            }
        }
    }

    return counter;
}